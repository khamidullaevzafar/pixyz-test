import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    formValues: {}
  },
  mutations: {
    setFormValues(state, payload) {
      Vue.set(state.formValues, payload.name, payload.value)
    }
  },
  getters: {
    getFormValues(state) {
      return state.formValues
    }
  },
  actions: {
    
  }

})